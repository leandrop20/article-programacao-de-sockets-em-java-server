package br.com.devmedia;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketServer {

	private static final int PORT = 1234;
	
	public static void main(String[] args) {
		try {
			ServerSocket server = new ServerSocket(PORT);
			Socket socket = server.accept();
			DataInputStream dis = new DataInputStream(socket.getInputStream());
			DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
			
			File file = new File("E:\\imagens\\mario.png");
			byte[] buffer = new byte[(int) file.length()];
			
			/*String message = dis.readUTF();
			
			System.out.println("Servidor: Mensagem recebida => \"" + message + "\"");
			
			message = "Servidor - Mensagem recebida com sucesso �s " + new Date();
			
			System.out.println("Servidor: Mensagem enviada => \"" + message + "\"");
			
			dos.writeUTF(message);*/
			
			FileInputStream fis = new FileInputStream(file);
			BufferedInputStream bis = new BufferedInputStream(fis);
			bis.read(buffer, 0, buffer.length);
			
			System.out.println("Enviando arquivo \"" + file.getName() + "\"");
			
			dos.write(buffer, 0, buffer.length);
			dos.flush();
			
//			bis.close();
			dis.close();
			dos.close();
			socket.close();
			server.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}